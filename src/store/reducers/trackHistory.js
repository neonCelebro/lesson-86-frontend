import {FETCH_TRACKHISTORY_SUCCESS} from "../actions/actionTypes";

const initialState = {
    history: []
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_TRACKHISTORY_SUCCESS:
            return {...state, history: action.history};
        default:
            return state;
    }
};

export default reducer;