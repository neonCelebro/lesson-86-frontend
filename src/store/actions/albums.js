import axios from '../../axios-api';
import {FETCH_ALBUMS_SUCCESS} from "./actionTypes";

export const fetchAlbumsSuccess = albums => {
    return {type: FETCH_ALBUMS_SUCCESS, albums};
};

export const fetchAlbums = (idArtist) => {
    return dispatch => {
        axios.get(idArtist).then(
            response => dispatch(fetchAlbumsSuccess(response.data))
        );
    }
};