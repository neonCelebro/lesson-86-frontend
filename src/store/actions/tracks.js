import axios from '../../axios-api';
import {FETCH_TRACKS_SUCCESS} from "./actionTypes";

export const fetchTracksSuccess = tracks => {
    return {type: FETCH_TRACKS_SUCCESS, tracks};
};

export const fetchTracks = (idAlbum) => {
    return dispatch => {
        axios.get(idAlbum).then(
            response => dispatch(fetchTracksSuccess(response.data))
        );
    }
};

export const addTrackInHistory = (idTrack) => {
    return (dispatch, getState) => {
        try{
            const token = getState().users.user.token;
            const track = idTrack;
            const user = getState().users.user._id;
            const headers = {token};
            axios.post('/track_history',{track, user}, {headers}).then(
                response => console.log('add to history'),
                error => console.log ('not add in history')
            );
        } catch(err) {
            console.log('track not added in history')
        }

    }
};