import axios from '../../axios-api';
import {FETCH_TRACKHISTORY_SUCCESS} from "./actionTypes";
import {push} from 'react-router-redux';

export const fetchTrackHistorySuccess = history => {
    return {type: FETCH_TRACKHISTORY_SUCCESS, history};
};

export const fetchHistory = () => {
    return (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {token};
            axios.get('/track_history', {headers}).then(
                response => dispatch(fetchTrackHistorySuccess(response.data)),
                error => dispatch(push('/register'))
            )
        } catch (err) {
            dispatch(push('/register'))
        }

    }
};