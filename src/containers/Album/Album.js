import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";

import ListItem from '../../components/UI/ListItem/ListItem';
import {fetchAlbums} from "../../store/actions/albums";

class Albums extends Component {
    componentDidMount() {
        this.props.onFetchAlbums(this.props.location.pathname);
    }

    render() {
        return (
            <Fragment>
                <PageHeader style={{borderColor: '#6dac8a'}}>
                    Music FM / Artists / Albums
                </PageHeader>
                {this.props.albums.map(album => (
                    <ListItem
                        key={album._id}
                        id={album._id}
                        name={album.name}
                        image={album.image}
                        link='tracks'
                    >
                        <span style={{textAlign: 'center'}}> | Год: {album.year}</span>
                    </ListItem>
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        albums: state.albums.albums
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchAlbums: idArtist => dispatch(fetchAlbums(idArtist))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Albums);