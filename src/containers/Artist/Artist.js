import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";
import {fetchArtists} from "../../store/actions/artists";

import ListItem from '../../components/UI/ListItem/ListItem';

class Artists extends Component {
  componentDidMount() {
    this.props.onFetchArtists();
  }

  render() {
    return (
      <Fragment>
        <PageHeader style={{borderColor: '#6dac8a'}}>
          Music FM / Artists
        </PageHeader>
        {this.props.artists.map(artist => (
          <ListItem
            key={artist._id}
            id={artist._id}
            name={artist.name}
            image={artist.image}
            link='albums'
          />
        ))}
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    artists: state.artists.artists
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchArtists: () => dispatch(fetchArtists())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Artists);