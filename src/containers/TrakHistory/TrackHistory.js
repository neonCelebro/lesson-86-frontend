import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";
import ListItem from '../../components/UI/ListItem/ListItem';
import {fetchHistory} from "../../store/actions/trackHistory";

class TrackHistory extends Component {
    componentDidMount() {
        this.props.fetchHistory();
    }

    render() {
        return (
            <Fragment>
                <PageHeader style={{borderColor: '#6dac8a'}}>
                    Music FM / Track history
                </PageHeader>
                {this.props.history.map((history) => {
                    return (
                        <ListItem
                            key={history._id}
                            id={history._id}
                            image={history.track.album.image}
                        >
                            <p style={{textAlign: 'center'}}>Исполнитель: {history.track.album.author.name}</p>
                            <p style={{textAlign: 'center'}}>Трэк: {history.track.name}</p>
                            <p style={{textAlign: 'center'}}>дата: {history.datetime}</p>
                        </ListItem>
                    )
                })}
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        history: state.history.history
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchHistory: () => dispatch(fetchHistory())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(TrackHistory);