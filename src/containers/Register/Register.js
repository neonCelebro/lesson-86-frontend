import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Alert, Col, ControlLabel, Form, FormControl, FormGroup, HelpBlock, PageHeader} from "react-bootstrap";
import {loginUser, registerUser} from "../../store/actions/users";
import ActionCreate from 'material-ui/svg-icons/action/assignment';
import ActionLogIn from 'material-ui/svg-icons/action/assignment-turned-in';
import {FlatButton, MuiThemeProvider} from "material-ui";

class Register extends Component {
  state = {
    username: '',
    password: ''
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  registerButtonHandler = event => {
    event.preventDefault();
    this.props.registerUser(this.state);
  };

    loginButtonHandler = event => {
        event.preventDefault();
        this.props.loginUser(this.state);
    };

  fieldHasError = fieldName => {
    return this.props.registerError && this.props.registerError.errors[fieldName];
  };

  render() {
    return (
      <Fragment>
          <MuiThemeProvider>
        <PageHeader style={{borderColor: '#6dac8a'}}>Register new user</PageHeader>
        <Form horizontal onSubmit={this.submitFormHandler}>

          <FormGroup
              controlId="username"
              validationState={this.fieldHasError('username') ? 'error': null}
          >
              {this.props.loginError &&
              <Alert bsStyle="danger">{this.props.loginError.message}</Alert>
              }
            <Col componentClass={ControlLabel} sm={2}>
              Username
            </Col>
            <Col sm={4}>
              <FormControl
                type="text"
                placeholder="Enter username"
                name="username"
                value={this.state.username}
                onChange={this.inputChangeHandler}
                autoComplete="off"
              />
              {this.fieldHasError('username') &&
                <HelpBlock>{this.props.registerError.errors.username.message}</HelpBlock>
              }
            </Col>
          </FormGroup>

          <FormGroup
              controlId="password"
              validationState={this.fieldHasError('password') ? 'error': null}
          >
              {this.props.error &&
              <Alert bsStyle="danger">{this.props.loginError.message}</Alert>
              }
            <Col componentClass={ControlLabel} sm={2}>
              Password
            </Col>
            <Col sm={4}>
              <FormControl
                type="password"
                placeholder="Enter password"
                name="password"
                value={this.state.password}
                onChange={this.inputChangeHandler}
                autoComplete="new-username"
              />
                {this.fieldHasError('password') &&
                <HelpBlock>{this.props.registerError.errors.password.message}</HelpBlock>
                }
            </Col>
          </FormGroup>

          <FormGroup>
            <Col smOffset={2} sm={3} style={{width: '20%'}}>
                <FlatButton
                    label="Create account"
                    labelPosition="after"
                    primary={true}
                    icon={<ActionCreate />}
                    onClick={this.registerButtonHandler}
                />
            </Col>
              <Col smOffset={0} sm={3} >
                  <FlatButton
                      style={{width: '48%'}}
                      label="Log in"
                      labelPosition="after"
                      primary={true}
                      icon={<ActionLogIn />}
                      onClick={this.loginButtonHandler}
                  />
              </Col>
          </FormGroup>
        </Form>
          </MuiThemeProvider>
      </Fragment>

    )
  }
}

const mapStateToProps = state => ({
    registerError: state.users.registerError,
    loginError: state.users.loginError
});

const mapDispatchToProps = dispatch => ({
    registerUser: userData => dispatch(registerUser(userData)),
    loginUser : userData => dispatch(loginUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);