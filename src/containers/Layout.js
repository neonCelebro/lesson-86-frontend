import React, {Fragment} from 'react';
import {connect} from 'react-redux';

import Toolbar from "../components/UI/Toolbar/Toolbar";
import {fetchLogoutUser} from "../store/actions/users";

const Layout = props => (
    <Fragment>
        <header>
            <Toolbar user={props.user} logout={props.logout}/>
        </header>
        <main className="container">
            {props.children}
        </main>
    </Fragment>
);

const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispathToProps = dispatch => ({
    logout: () => dispatch(fetchLogoutUser())
});

export default connect(mapStateToProps, mapDispathToProps)(Layout);