import React, {Component} from 'react';
import {Route, Switch} from "react-router-dom";

import Artists from "./containers/Artist/Artist";
import Register from "./containers/Register/Register";
import Layout from "./containers/Layout";
import Albums from "./containers/Album/Album";
import Tracks from "./containers/Track/Track";
import TrackHistory from "./containers/TrakHistory/TrackHistory";

class App extends Component {
  render() {
    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={Artists} />
                <Route path="/register" exact component={Register} />
                <Route path="/albums/" component={Albums} />
                <Route path="/tracks/" component={Tracks} />
                <Route path="/track_history/" component={TrackHistory} />
            </Switch>
        </Layout>
    );
  }
}

export default App;