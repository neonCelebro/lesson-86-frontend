import React from 'react';
import {Image, Panel} from "react-bootstrap";
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';

import config from '../../../config';

import notFound from '../../../assets/images/not-found.png';

const ProductListItem = props => {
    let image = notFound;

    if (props.image) {
        image = config.apiUrl + '/uploads/' + props.image;
    }

    return (
        <Panel style={{
            width: '30%',
            display: 'inline-block',
            marginRight: '2%',
            backgroundColor: 'rgba(0, 0, 0, 0.25)',
            borderColor: 'rgba(0, 0, 0, 0.25)'
        }}>
            <Panel.Body>
                <Image
                    style={{
                        width: '100px',
                        height: '80px',
                        marginRight: '10px',
                        objectFit: 'cover',
                        backgroundColor: 'rgba(0, 0, 0, 0.25)',
                        borderColor: '#373737'
                    }}
                    src={image}
                    thumbnail
                />
                <Link style={{color: '#6dac8a'}} to={`/${props.link}/` + props.id}>
                    {props.name}
                </Link>
                {props.children}
            </Panel.Body>
        </Panel>
    );
};

ProductListItem.propTypes = {
    id: PropTypes.string.isRequired,
    image: PropTypes.string,
    link: PropTypes.string
};

export default ProductListItem;