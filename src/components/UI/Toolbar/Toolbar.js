import React from 'react';
import {Nav, Navbar} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import {FlatButton, IconButton, MuiThemeProvider} from "material-ui";
import ActionHome from 'material-ui/svg-icons/action/home';
import ActionMusic from 'material-ui/svg-icons/action/settings-voice';
import ActionSignUp from 'material-ui/svg-icons/action/account-box';
import ActionUser from 'material-ui/svg-icons/action/settings-applications';
import ActionLogout from 'material-ui/svg-icons/action/exit-to-app';
import NavItem from "react-bootstrap/es/NavItem";


const Toolbar = props => (
    <MuiThemeProvider>
      <Navbar bsStyle="inverse" >
    <Navbar.Header>
        <LinkContainer to="/" exact>
            <IconButton iconStyle={{color : "rgb(0, 188, 212)"}} tooltip="Go home">
                <ActionHome />
            </IconButton>
        </LinkContainer>
      <Navbar.Toggle />
    </Navbar.Header>
    <Navbar.Collapse>
      <Nav pullRight>
        <LinkContainer  to="/" exact>
            <FlatButton
                label="Music"
                labelPosition="after"
                primary={true}
                icon={<ActionMusic />}>
            </FlatButton>
        </LinkContainer>
          { !props.user ?
              <LinkContainer to="/register" exact>
            <FlatButton
                label="Sign Up"
                labelPosition="after"
                primary={true}
                icon={<ActionSignUp />}
            >
            </FlatButton>
        </LinkContainer>
              : null}
          { props.user ?
              <LinkContainer to="/track_history" exact>
                  <FlatButton
                      label={`welcome ${props.user.username}!`}
                      labelPosition="after"
                      primary={true}
                      icon={<ActionUser />}
                  >
                  </FlatButton>
              </LinkContainer>
              : null}
          { props.user ?
              <FlatButton
                  label="log out"
                  labelPosition="after"
                  primary={true}
                  icon={<ActionLogout />}
                  onClick={props.logout}
              >
              </FlatButton>
              : null}
      </Nav>
    </Navbar.Collapse>
  </Navbar>
    </MuiThemeProvider>
);

export default Toolbar;