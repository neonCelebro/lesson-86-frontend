import React, {Component} from 'react';
import ReactPlayer from 'react-player';
import PropTypes from 'prop-types';
import {List, ListItem, makeSelectable} from 'material-ui/List';
import ActionPlay from 'material-ui/svg-icons/action/play-for-work';
import Subheader from 'material-ui/Subheader';
import {darkBaseTheme, getMuiTheme, MuiThemeProvider} from "material-ui/styles/index";


let SelectableList = makeSelectable(List);

function wrapState(ComposedComponent, props) {
    return class SelectableList extends Component {

        static propTypes = {
            children: PropTypes.node.isRequired,
            defaultValue: PropTypes.number.isRequired,
        };

        render() {
            return (
                <MuiThemeProvider muiTheme={getMuiTheme(darkBaseTheme)}>
                    <ComposedComponent>
                        {this.props.children}
                    </ComposedComponent>
                </MuiThemeProvider>

            );
        }
    };
}

SelectableList = wrapState(SelectableList);

const ListExampleSelectable = (props, state) => (
    <SelectableList defaultValue={3}>
        <Subheader>{props.title}</Subheader>
        {props.tracks.map((track) => {
            return (
                <ListItem
                    onClick={()=> props.trackHendler(track._id)}
                    value={1}
                    primaryTogglesNestedList={true}
                    key={track._id}
                    nestedlevel={1}
                    style={{width: '300px', color : "rgb(0, 188, 212)"}}
                    primaryText={track.name}
                    leftIcon={<ActionPlay />}
                    nestedItems={[
                       <ReactPlayer key={track._id} url={track.url} playing />
                    ]}
                />
            )
        })}
    </SelectableList>
);


export default ListExampleSelectable;
